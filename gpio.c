#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <time.h>

#define GPIO_SYSFS_PATH	"/sys/class/gpio"

#define DIR_IN	1
#define DIR_OUT	2

#define EDGE_NONE	0
#define EDGE_RISE	1
#define EDGE_FALL	2
#define EDGE_BOTH	3

char g_filename[64];
char g_buf[1];

void syntax(char *command)
{
	printf ("%s pin [opt] [val]\n", command);
	printf ("\tpin: pin number\n");
	printf ("\topt: r w i o d u f b p\n");
	printf ("\t\tr: read gpio pin current value\n");
	printf ("\t\tw: write [val] to gpio pin\n");
	printf ("\t\ti: set gpio pin direction as input\n");
	printf ("\t\to: set gpio pin direction as output\n");
	printf ("\t\td: get gpio pin direction\n");
	printf ("\t\tu: gpio edge rising test\n");
	printf ("\t\tf: gpio edge falling test\n");
	printf ("\t\tb: gpio edge both test\n");
	printf ("\t\tp: gpio press test\n");
}

int gpio_get_edge(int pin)
{
	int fd;

	sprintf(g_filename, "%s/gpio%d/edge", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_RDONLY);
	if (fd) {
		read(fd, g_buf, 1);
		close(fd);
		if (g_buf[0] == 'n') {
			return EDGE_NONE;
		} else if (g_buf[0] == 'b') {
			return EDGE_BOTH;
		} else if (g_buf[0] == 'r') {
			return EDGE_RISE;
		} else if (g_buf[0] == 'f') {
			return EDGE_FALL;
		}
	} else {
		printf("open file [%s] fail\n", g_filename);
		return -1;
	}
}

int gpio_press(int pin)
{
	int fd, i, ret;
	unsigned char c, pre;
	struct pollfd pfd;
	struct timespec tt1, tt2;
	long sec, nanosec;


	if (gpio_get_edge(pin) == EDGE_NONE) {
		printf("gpio %d edge was none\n", pin);
		return -1;
	}

	sprintf(g_filename, "%s/gpio%d/value", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_RDONLY);
	pfd.fd = fd;
	pfd.events = POLLPRI;

	c = '0';
	while(c == '0') {	// wait release key
		lseek(fd, 0, SEEK_SET);
		ret = poll(&pfd, 1, -1);
		read(fd, &c, 1);
	}

	i = 0;
	while(1) {
		lseek(fd, 0, SEEK_SET);
		ret = poll(&pfd, 1, -1);
		read(fd, &c, 1);
		if(ret == 0) {
			printf("Timeout\n");
		} else {
			if (c == '0') {
				clock_gettime(CLOCK_REALTIME, &tt1);
				pre = 1;
				printf("Press.\n");
			} else {
				if (pre) {
					clock_gettime(CLOCK_REALTIME, &tt2);
					sec = tt2.tv_sec - tt1.tv_sec;
					nanosec = tt2.tv_nsec - tt1.tv_nsec;
					if (nanosec < 0 ) {
						--sec;
						nanosec += 1000000000;
					}
					printf("release after %d.%09d seconds!\n", sec, nanosec);
					if (++i == 10) {
						break;
					}
				}
				pre = 0;
			}
		}
	}
	close(fd);
}

int gpio_poll(int pin)
{
	int fd, i, ret;
	unsigned char c;
	struct pollfd pfd;

	if (gpio_get_edge(pin) == EDGE_NONE) {
		printf("gpio %d edge was none\n", pin);
		return -1;
	}

	sprintf(g_filename, "%s/gpio%d/value", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_RDONLY);
	pfd.fd = fd;
	pfd.events = POLLPRI;

	{	// skip first event
		lseek(fd, 0, SEEK_SET);
		ret = poll(&pfd, 1, -1);
		read(fd, &c, 1);
	}

	for(i = 1; i <= 10; i++) {
		lseek(fd, 0, SEEK_SET);
		ret = poll(&pfd, 1, -1);
		read(fd, &c, 1);
		if(ret == 0) {
			printf("Timeout\n");
			--i;
		} else {
			printf("interupt count = %d, ", i);
			printf("gpio value = %c\n", c);
		}
	}
	close(fd);
}

int gpio_set_edge(int pin, int edge)
{
	int fd, ret;
	struct stat s;

	sprintf(g_filename, "%s/gpio%d/edge", GPIO_SYSFS_PATH, pin);
	ret = stat(g_filename, &s);
	if(-1 == ret) {	// not exist
		printf("gpio pin %d could with out interrupt support\n", pin);
		return -1;
	}

	fd = open(g_filename, O_WRONLY);
	if (fd) {
		if (edge == EDGE_BOTH) {
			write(fd, "both", 4);
		} else if (edge == EDGE_RISE) {
			write(fd, "rising", 6);
		} else if (edge == EDGE_FALL) {
			write(fd, "falling", 7);
		}
		close(fd);
	} else {
		printf("can't open edge file of gpio pin %d\n", pin);
		return -1;
	}
	return 0;
}

int gpio_get_direction(int pin)
{
	int fd;

	sprintf(g_filename, "%s/gpio%d/direction", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_RDONLY);
	if (fd) {
		read(fd, g_buf, 1);
		close(fd);
		return (g_buf[0] == 'i')? DIR_IN : DIR_OUT;
	} else {
		printf("open file [%s] fail\n", g_filename);
		return -1;
	}
}

int gpio_set_direction(int pin, int dir)
{
	int fd;

	sprintf(g_filename, "%s/gpio%d/direction", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_WRONLY);
	if (fd) {
		if (dir == DIR_IN) {
			write(fd, "in", 2);
		} else {
			write(fd, "out", 3);
		}
		close(fd);
	} else {
		printf("can't open direction file of gpio pin %d\n", pin);
		return -1;
	}
	return 0;
}

int gpio_export(int pin)
{
	char buf[4];
	int err, fd;
	struct stat s;

	sprintf(g_filename, "%s/gpio%d", GPIO_SYSFS_PATH, pin);
	err = stat(g_filename, &s);
	if(-1 == err) {	// not exist
		fd = open(GPIO_SYSFS_PATH "/export", O_WRONLY);
		if (fd) {
			sprintf(buf, "%d", pin);
			write(fd, buf, strlen(buf));
			close(fd);
		}
		err = stat(g_filename, &s);
		if(-1 == err) {	// not exist
			printf("can't set as gpio [%d]\n", pin);
			return -1;
		}
	}
}

void gpio_read(int pin)
{
	int fd;

	sprintf(g_filename, "%s/gpio%d/value", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_RDONLY);
	if (fd) {
		read(fd, g_buf, 1);
		printf("read gpio%d: %c\n", pin, g_buf[0]);
		close(fd);
	} else {
		printf("can't read gpio%d\n", pin);
	}
}

void gpio_write(int pin, int value)
{
	int fd, ret;

	if (value < 0 || value > 1) {
		printf("val should be 0 or 1\n", pin);
		return;
	}

	ret = gpio_get_direction(pin);
	if (ret != DIR_OUT) {
		printf("gpio %d was not a output pin\n", pin);
		return;
	}

	sprintf(g_filename, "%s/gpio%d/value", GPIO_SYSFS_PATH, pin);
	fd = open(g_filename, O_WRONLY);
	if (fd) {
		sprintf(g_buf, "%d", value);
		write(fd, g_buf, 1);
		printf("write gpio%d: %c\n", pin, g_buf[0]);
		close(fd);
	} else {
		printf("can't write gpio%d\n", pin);
	}
}

int main(int argc, char **argv)
{
	int pin, value, ret;

	if (argc < 3) {
		syntax(argv[0]);
		return -1;
	}

	// [1] pin
	pin = atoi(argv[1]);
	ret = gpio_export(pin);
	if (ret != 0) {
		return ret;
	}

	// [2] operator
	switch (argv[2][0]) {
		case 'r':
			gpio_read(pin);
			break;
		case 'w':
        	if (argc < 4) {
        		syntax(argv[0]);
        		return -1;
        	}
			value = atoi(argv[3]);
			gpio_write(pin, value);
			break;
		case 'i':
			gpio_set_direction(pin, DIR_IN);
			break;
		case 'o':
			gpio_set_direction(pin, DIR_OUT);
			break;
		case 'd':
			break;
		case 'u':
			gpio_set_direction(pin, DIR_IN);
			gpio_set_edge(pin, EDGE_RISE);
			break;
		case 'f':
			gpio_set_direction(pin, DIR_IN);
			gpio_set_edge(pin, EDGE_FALL);
			break;
		case 'b':
		case 'p':
			gpio_set_direction(pin, DIR_IN);
			gpio_set_edge(pin, EDGE_BOTH);
			break;
		default:
			syntax(argv[0]);
			return -1;
			break;
	}

	switch (argv[2][0]) {
		case 'i':
		case 'o':
		case 'd':
			ret = gpio_get_direction(pin);
			printf("gpio %d direction is %s\n", pin, (ret == DIR_IN) ? "IN" : "OUT");
			break;
		case 'u':
		case 'f':
		case 'b':
			gpio_poll(pin);
			break;
		case 'p':
			gpio_press(pin);
			break;
	}
	return 0;
}

